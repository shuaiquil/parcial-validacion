import static org.junit.jupiter.api.Assertions.*;

import java.text.DecimalFormat;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class EcuacionTest {

	@Test
	public void test1() throws Exception{
		Ecuacion ecuacion1 = new Ecuacion(1);		
		assertEquals(ecuacion1.cuadratica(-2), 4);		
	}
	
	@Test
	public void test2() throws Exception{
		Ecuacion ecuacion2 = new Ecuacion(3,6);
		assertEquals(ecuacion2.cuadratica(0), 0);
				
	}
	
	@Test
	public void test3() throws Exception{
		Ecuacion ecuacion3 = new Ecuacion(7,2,9);
		assertEquals(ecuacion3.cuadratica(1), 18);
	}
	
	@Test
	public void test4() throws Exception{
		Ecuacion ecuacion4 = new Ecuacion(-1);

		Assertions.assertThrows(Exception.class, () -> ecuacion4.raiz(3));	
	}
	
	@Test
	public void test5() throws Exception{
		Ecuacion ecuacion5 = new Ecuacion(7,5);
	
		assertEquals(ecuacion5.raiz(-2),-15.291502622129181);
	}
	
	@Test
	public void test6() throws Exception{
		Ecuacion ecuacion6 = new Ecuacion(3, -2, 0);
	
		assertEquals(ecuacion6.raiz(0),0);
	}
	
	

	
}
