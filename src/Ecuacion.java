import java.text.DecimalFormat;

public class Ecuacion {

	private double a;
	private double b;
	private double c;

	public Ecuacion(double a) {
		this.a = a;
		this.b = 0;
		this.c = 0;
	}

	public Ecuacion(double a, double b) {
		this.a = a;
		this.b = b;
		this.c = 0;
	}

	public Ecuacion(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	// M�todos
	public double cuadratica(double x) throws Exception {
		if (a != 0) {
			double p1 = this.a * Math.pow(x, 2);
			double p2 = this.b * x;
			double p3 = this.c;

			return p1 + p2 + p3;
		} else {
			throw new Exception("No se puede realizar la operacion");
		}
	}

	public double raiz(double x) throws Exception {

		if (a >= 0) {
			double p1 = x * Math.sqrt(this.a);
			double p2 = this.b * x;
			double p3 = this.c;
			
			return p1+ p2 +p3;
				
		} else {
			throw new Exception("No se puede realizar la operacion");
		}

	}
}
